
# Youtube Analyzer

## Steps to Dump the CSV Content to DB

1. Login to Mysql Console
2. create database Aggegator CHARACTER SET utf8 COLLATE utf8_general_ci;;
3. 

    create table `channel_info` ( 
         `rank` varchar(100) NOT NULL,
         `grade` varchar(100) NOT NULL,
         `channel_name` varchar(255) NOT NULL,
         `video_upload` int(11),
         `subscribers` int(11),
         `video_views` int(11)
        );

4.

     LOAD DATA LOCAL INFILE '<path/file.csv>' INTO TABLE channel_info FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 ROWS;

5. CSV File will be uploaded to Relational DB

## Steps to run the application

1. Open src/main/resources/application.yml
2. Change the required port,username,password for your Mysql.
3. Server will run on 9090 port.
4. in command propt execute -> 
5. `gradle clean build.`
6. After build succcess -> 
7. `java -jar build\libs\aggregatorapp-0.0.1-SNAPSHOT.jar`
8. Open a browser -> hit http://localhost:9090/ui
9. It will open the UI for Checking the Youtube pages.

## Steps to run the application as Docker

1. Open src/main/resources/application.yml
2. Change the jdbc url to host ip.
3. Change the required port,username,password for your Mysql.
4. Open command prompt. 
5. `gradle clean buildDocker`
6. After success - 
7. `docker run -it -d -p 9090:9090 com.youtube/aggregatorapp:0.0.1-SNAPSHOT`
8. Open a browser -> hit http://localhost:9090/ui
9. It will open the UI for Checking the Youtube pages.

## Devoloped API's
API to Fetch Channel Details with Filter, Limit, Offset and Sorting support
Request with all Filter , Limit Offet and Sorting :  

    curl -X GET   'http://localhost:9090/channels?limit=2&filter=equals(rank,1st)&orderby=subscribers,desc'  -H 'Content-Type: application/json'
 
 Response:

    {
    "metaInfo": {
        "total": 1,
        "limit": 2,
        "offset": 0
    },
    "results": [
        {
            "channelName": "Zee TV",
            "rank": "1st",
            "grade": "A++ ",
            "subscribers": "18752951",
            "videoUploads": "82757",
            "videoViews": "2147483647"
        }
    ]
}
## Limitation

1. Currently API only support Filter with Contains,Equals and StartsWith Filter. Need to add support for all columns.
2. Current API only support Sorting for Channel Name, Subscribers. Need to add support fot Rank also.
3. May be API and UI have some bugs needs to be fixed.


## Technologies
1. Java
2. Spring Boot(Backend)
3.  Thymleaf(UI)
4. Docker (Deployment)
![UI Screen for Youtube Statistics](https://he-s3.s3.amazonaws.com/media/uploads/ed0deec.JPG)






