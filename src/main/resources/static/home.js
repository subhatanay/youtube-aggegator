
var pageOptions = {
    currentPage: 0,
    totalRecords: 0,
    limit: 20
}
document.addEventListener("DOMContentLoaded", function () {
    document.querySelector("#generateSearch").addEventListener("click", function () {
        search();
    });
    document.querySelector("#prevPage").addEventListener("click", function () {
        changePage(false);
    });
    document.querySelector("#nextPage").addEventListener("click", function () {
        changePage(true);
    });
    fetchChannelInfos();
});
function search(options, nextpage) {
    var serachField = document.querySelector("#searchField").value;
    var searchMethod = document.querySelector("#methodField").value;
    var searchText = document.querySelector("#searchText").value;

    if (serachField == -1) {
        return toast("Select a Search Field");
    }
    if (searchMethod == -1) {
        return toast("Select a Search Method");
    }

    if (options) {
        options["filter"] = searchText !== "" ? searchMethod + "(" + serachField + "," + encodeURIComponent(searchText) + ")" : "";
        fetchChannelInfos(options, nextpage);
    } else {
        var options = {
            "filter": searchText !== "" ? searchMethod + "(" + serachField + "," + encodeURIComponent(searchText) + ")" : "",
            "limit": 20
        };
        fetchChannelInfos(options);
    }
}
function changePage(isNextPage) {
    var options = {};
    if (isNextPage) {
        options["limit"] = 20;
        if ((pageOptions["offset"] + pageOptions["limit"]) <= pageOptions["totalRecords"]) {
            options["offset"] = pageOptions["offset"] + pageOptions["limit"];
            options["limit"] = 20;
            search(options, true);
        }
    } else {
        if ((pageOptions["offset"] - pageOptions["limit"]) >= 0) {
            options["offset"] = pageOptions["offset"] - pageOptions["limit"];
            options["limit"] = 20;
            search(options, false);
        }
    }
}
function fetchChannelInfos(options, nextPage) {
    var url = "/channels?";
    if (options) {
        for (var o in options) {
            url = url + o + "=" + options[o] + "&";
        }
        url = url.substring(0, url.length - 1);
    } else {
        url = "channels?limit=20";
    }


    $.ajax({
        async: false,
        type: "GET",
        url: url,
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        success: function (data) {
            if (nextPage === true) {
                pageOptions["currentPage"] = pageOptions["currentPage"] >= pageOptions["noOfPage"] ? pageOptions["currentPage"] : pageOptions["currentPage"] + 1;
            } else if (nextPage === false) {
                pageOptions["currentPage"] = pageOptions["currentPage"] <= 0 ? 0 : pageOptions["currentPage"] - 1;
            }
            poulateGrid(data);
            setMetaInfo(data);
        },
        error: function (data) {
            if (data.status === 404) {
                toast("No Channels found");
            }
            if ($.fn.dataTable.isDataTable('#channelTable1'))
            {
                $("#channelTable1").dataTable().fnDestroy();
            }
            if (data.status === 500) {
                toast("Oops some error occured...");
            }
            var table = $("#channelTable1").DataTable({
                "language": {
                    "emptyTable": 'No Channels found',
                    "zeroRecords": 'No Channels found'
                },
                "columnDefs": [
                    {orderSequence: ["desc", "asc"],
                        aTargets: ['_all']}
                ]

            });
            $("#channelTable1_filter").remove();
            $(".dataTables_paginate").remove();
            $(".dataTables_info").remove();
            $(".dataTables_info").remove();
            $(".dataTables_length").remove();
            F

        }
    });
}
function setMetaInfo(data) {
    var meta = data["metaInfo"];
    pageOptions["totalRecords"] = parseInt(meta.total);
    pageOptions["noOfPage"] = Math.round(parseInt(meta.total) / parseInt(meta.limit));
    pageOptions["limit"] = meta.limit;
    pageOptions["offset"] = meta.offset;
    $("#pageInfo").html("#Page : <b>" + pageOptions["currentPage"] + "/" + pageOptions["noOfPage"] + "</b>");
}
function poulateGrid(channels) {
    if ($.fn.dataTable.isDataTable('#channelTable1'))
    {
        $("#channelTable1").dataTable().fnDestroy();
    }
    var tableBody = document.querySelector("#channelTableBody");
    tableBody.innerHTML = "";
    var innerTableBody = "";
    document.querySelector("#channelTable").style = "overflow-x:auto";
    document.querySelector("#tableError").style.display = "block";
    if (channels && channels["results"].length > 0) {
        document.querySelector("#tableError").style.display = "none";
        for (var i = 0; i < channels["results"].length; i++) {
            innerTableBody = innerTableBody + "<tr><td class='mdl-data-table__cell--non-numeric'>" + channels["results"][i]["channelName"] + "</td><td class='mdl-data-table__cell--non-numeric'>" + channels["results"][i]["rank"] + "</td><td class='mdl-data-table__cell--non-numeric'>" + channels["results"][i]["grade"] + "</td><td class='mdl-data-table__cell--non-numeric'>" + channels["results"][i]["subscribers"] + "</td><td class='mdl-data-table__cell--non-numeric'>" + channels["results"][i]["videoViews"] + "</td><td class='mdl-data-table__cell--non-numeric'>" + channels["results"][i]["videoUploads"] + "</td></tr>";
        }

        tableBody.innerHTML = innerTableBody;

        var table = $("#channelTable1").DataTable({
            "language": {
                "emptyTable": 'No Channels found',
                "zeroRecords": 'No Channels found'
            },
            "columnDefs": [
                {orderSequence: ["desc", "asc"],
                    aTargets: ['_all']}
            ]

        });
        $("#channelTable1_filter").remove();
        $(".dataTables_paginate").remove();
        $(".dataTables_info").remove();
        $(".dataTables_info").remove();
        $(".dataTables_length").remove();
    } else {
        document.querySelector("#channelTable").style = "display:none;overflow-x:auto";
        document.querySelector("#tableError").innerText = "No Channel Info found.";
        document.querySelector("#tableError").style.display = "block";
    }
}

function toast(message, color) {
    var snackbarContainer = document.querySelector('#bam-toast');
    var data1 = {
        message: message,
        timeout: 3000
    };

    snackbarContainer.style.alignContent = "center";
    snackbarContainer.classList.remove('mdl-color--green-700');
    snackbarContainer.classList.remove('mdl-color--red-700');
    if (color) {
        snackbarContainer.classList.add('mdl-color--green-700');
    } else {
        snackbarContainer.classList.add('mdl-color--red-700');
    }
    snackbarContainer.MaterialSnackbar.showSnackbar(data1);

}


