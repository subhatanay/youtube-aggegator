/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.youtube.aggregatorapp.models;

import java.util.List;

/**
 *
 * @author subhajgh
 */
public class ResultModel {
    private MetaData metaInfo;
    private List<ChannelInfo> results;
    private String error;

    /**
     * @return the metaInfo
     */
    public MetaData getMetaInfo() {
        return metaInfo;
    }

    /**
     * @param metaInfo the metaInfo to set
     */
    public void setMetaInfo(MetaData metaInfo) {
        this.metaInfo = metaInfo;
    }

    /**
     * @return the results
     */
    public List<ChannelInfo> getResults() {
        return results;
    }

    /**
     * @param results the results to set
     */
    public void setResults(List<ChannelInfo> results) {
        this.results = results;
    }

    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }
    
}
