/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.youtube.aggregatorapp.models;

/**
 *
 * @author subhajgh
 */
public class Filter {
    private String filterSQL;
    private int limit;
    private int offset;
    private String orderBySQL;
    private String errorText;

    /**
     * @return the filterSQL
     */
    public String getFilterSQL() {
        return filterSQL;
    }

    /**
     * @param filterSQL the filterSQL to set
     */
    public void setFilterSQL(String filterSQL) {
        this.filterSQL = filterSQL;
    }

    /**
     * @return the limit
     */
    public int getLimit() {
        return limit;
    }

    /**
     * @param limit the limit to set
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * @return the offset
     */
    public int getOffset() {
        return offset;
    }

    /**
     * @param offset the offset to set
     */
    public void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * @return the errorText
     */
    public String getErrorText() {
        return errorText;
    }

    /**
     * @param errorText the errorText to set
     */
    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    /**
     * @return the orderBySQL
     */
    public String getOrderBySQL() {
        return orderBySQL;
    }

    /**
     * @param orderBySQL the orderBySQL to set
     */
    public void setOrderBySQL(String orderBySQL) {
        this.orderBySQL = orderBySQL;
    }
}
