/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.youtube.aggregatorapp.models;

/**
 *
 * @author subhajgh
 */
public class ChannelInfo {
    private String channelName;
    private String rank;
    private String grade;
    private String subscribers;
    private String videoUploads;
    private String videoViews;

    /**
     * @return the channelName
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * @param channelName the channelName to set
     */
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    /**
     * @return the rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(String rank) {
        this.rank = rank;
    }

    /**
     * @return the grade
     */
    public String getGrade() {
        return grade;
    }

    /**
     * @param grade the grade to set
     */
    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * @return the subscribers
     */
    public String getSubscribers() {
        return subscribers;
    }

    /**
     * @param subscribers the subscribers to set
     */
    public void setSubscribers(String subscribers) {
        this.subscribers = subscribers;
    }

    /**
     * @return the videoUploads
     */
    public String getVideoUploads() {
        return videoUploads;
    }

    /**
     * @param videoUploads the videoUploads to set
     */
    public void setVideoUploads(String videoUploads) {
        this.videoUploads = videoUploads;
    }

    /**
     * @return the videoViews
     */
    public String getVideoViews() {
        return videoViews;
    }

    /**
     * @param videoViews the videoViews to set
     */
    public void setVideoViews(String videoViews) {
        this.videoViews = videoViews;
    }
            
    
}
