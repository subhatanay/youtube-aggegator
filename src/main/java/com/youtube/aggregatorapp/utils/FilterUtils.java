/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.youtube.aggregatorapp.utils;

import com.youtube.aggregatorapp.models.Filter;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author subhajgh
 */
public class FilterUtils {

    static Logger logger = LoggerFactory.getLogger(FilterUtils.class);
    /*
        Filter to parse the filter string to sql
         ex: contains(column_key,acde)|contains(column_key,acde)|contains(column_key,acde)
         
         where column_key = a
        
     */
    private final static String CONTAINS = "contains(";
    private final static String STARTS_WITH = "starts(";
    private final static String EQUALS = "equals(";
    private final static String FILTER_SEPERATOR = "\\|";

    public static Filter parseFilter(String filter, int limit, int offset, String sortorder) {
        List<String> mapColumnKey = Arrays.asList(new String[]{"channel_name", "rank", "grade"});
        List<String> sortColumnKey = Arrays.asList(new String[]{"channel_name", "rank", "grade", "subscribers", "video_upload", "video_views"});
        Filter filterObj = new Filter();
        filterObj.setLimit(limit);
        filterObj.setOffset(offset);
        StringBuilder filterString = new StringBuilder("");
        if (!"".equals(sortorder)) {
            
            StringBuilder sortFilter = new StringBuilder("");
            List<String> sortSplitArr = Arrays.asList(sortorder.split(FILTER_SEPERATOR));
            sortSplitArr.stream().forEach(str -> {
                System.out.println(str);
                String colSplit[] = str.split(",");
                if (colSplit.length == 2) {
                    if (sortColumnKey.contains(colSplit[0])) {
                        sortFilter.append("  ").append(colSplit[0]).append(" ").append(colSplit[1]).append("");
                    } else {
                        logger.error("Filter Error -> Error in parsing sort order");
                        filterObj.setErrorText("Invalid sort query");
                    }
                }
            });
            if (sortFilter.length() > 0 && filterObj.getErrorText() == null) {
//                sortFilter.delete(sortFilter.lastIndexOf(","), filterString.length());
                filterObj.setOrderBySQL(sortFilter.toString());
            } else {
                logger.error("Provided filter query is invalid");
                filterObj.setErrorText("Invalid filter query");
                return filterObj;
            }
        } 
        if (!"".equals(filter)) {
            List<String> filterSplitArr = Arrays.asList(filter.split(FILTER_SEPERATOR));
            filterSplitArr.stream().forEach(str -> {
                if (str.startsWith(CONTAINS)) {
                    String columnSearch = str.substring(str.indexOf("(") + 1, str.indexOf(")"));
                    String colSplit[] = columnSearch.split(",");
                    if (colSplit.length == 2) {
                        if (mapColumnKey.contains(colSplit[0])) {
                            filterString.append(" ").append(colSplit[0]).append(" LIKE '%").append(colSplit[1]).append("%' and ");
                        } else {
                            logger.error("Filter Error -> " + colSplit[0] + " column not found");
                            filterObj.setErrorText("Invalid filter query");
                        }
                    } else {
                        logger.error("Filter Error -> Error in parsing filter");
                        filterObj.setErrorText("Invalid filter query");
                    }
                } else if (str.startsWith(EQUALS)) {
                    String columnSearch = str.substring(str.indexOf("(") + 1, str.indexOf(")"));
                    String colSplit[] = columnSearch.split(",");
                    if (colSplit.length == 2) {
                        if (mapColumnKey.contains(colSplit[0])) {
                            filterString.append(" ").append(colSplit[0]).append(" = '").append(colSplit[1]).append("' and ");
                        } else {
                            logger.error("Filter Error -> " + colSplit[0] + " column not found");
                            filterObj.setErrorText("Invalid filter query");
                        }
                    } else {
                        logger.error("Filter Error -> Error in parsing filter");
                        filterObj.setErrorText("Invalid filter query");
                    }
                } else if (str.startsWith(STARTS_WITH)) {
                    String columnSearch = str.substring(str.indexOf("(") + 1, str.indexOf(")"));
                    String colSplit[] = columnSearch.split(",");
                    if (colSplit.length == 2) {
                        if (mapColumnKey.contains(colSplit[0])) {
                            filterString.append(" ").append(colSplit[0]).append(" LIKE '").append(colSplit[1]).append("%' and ");
                        } else {
                            logger.error("Filter Error -> " + colSplit[0] + " column not found");
                            filterObj.setErrorText("Invalid filter query");
                        }
                    } else {
                        logger.error("Filter Error -> Error in parsing filter");
                        filterObj.setErrorText("Invalid filter query");
                    }
                }
            });
            if (filterString.length() > 0 && filterObj.getErrorText() == null) {
                filterString.delete(filterString.lastIndexOf("and"), filterString.length());
                filterObj.setFilterSQL(filterString.toString());
            } else {
                logger.error("Provided filter query is invalid");
                filterObj.setErrorText("Invalid filter query");
            }
        }
        logger.info("Filter --> " + filterString.toString());
        return filterObj;

    }

}
