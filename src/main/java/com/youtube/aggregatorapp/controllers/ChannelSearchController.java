/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.youtube.aggregatorapp.controllers;

import com.youtube.aggregatorapp.models.ResponseModel;
import com.youtube.aggregatorapp.models.ResultModel;
import com.youtube.aggregatorapp.services.impl.ChannelInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author subhajgh
 */
@RestController
public class ChannelSearchController {

    Logger logger = LoggerFactory.getLogger(ChannelSearchController.class);
    @Autowired
    ChannelInfoService channelService;

    @RequestMapping("/channels")
    public Object findChannles(@RequestParam(value = "filter", defaultValue = "") String filter,
            @RequestParam(value = "orderby", defaultValue = "subscribers,desc") String orderBy,
            @RequestParam(value = "limit", defaultValue = "50") int limit,
            @RequestParam(value = "offset", defaultValue = "0") int offset) {
        try {
            ResultModel results = channelService.findChannels(filter, limit, offset,orderBy);
            if (results.getError() != null) {
                return new ResponseEntity<>(new ResponseModel("400", results.getError()), HttpStatus.BAD_REQUEST);
            }
            if (results.getMetaInfo().getTotal() > 0) {
                return new ResponseEntity<>(results, HttpStatus.OK);
            } else {
                logger.error("No channels found");
                return new ResponseEntity<>(new ResponseModel("500", "No channels found"), HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error while searhing channles . " + e.getMessage());
            return new ResponseEntity<>(new ResponseModel("500", "Internal Server Error"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
