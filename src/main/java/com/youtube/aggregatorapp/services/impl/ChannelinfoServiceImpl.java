/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.youtube.aggregatorapp.services.impl;

import com.youtube.aggregatorapp.dao.impl.ChannelInfoDao;
import com.youtube.aggregatorapp.models.ChannelInfo;
import com.youtube.aggregatorapp.models.Filter;
import com.youtube.aggregatorapp.models.MetaData;
import com.youtube.aggregatorapp.models.ResultModel;
import com.youtube.aggregatorapp.utils.FilterUtils;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author subhajgh
 */
@Service
public class ChannelinfoServiceImpl implements ChannelInfoService {

    @Autowired
    ChannelInfoDao channelDao;

    @Override
    public ResultModel findChannels(String filter, int limit, int offset, String sortOrder) {
        Filter filterContent = FilterUtils.parseFilter(filter, limit, offset, sortOrder);
        ResultModel resultInfo = new ResultModel();

        if (filterContent.getErrorText() == null) {
            MetaData meta = new MetaData();
            meta.setTotal(channelDao.totalChannels(filterContent));
            if (offset > meta.getTotal()) {
                offset = (int) meta.getTotal();
            } else if (offset<=0) {
                offset=0;
            }
            List<ChannelInfo> channelInfos = channelDao.fetchChannels(filterContent);

            meta.setLimit(limit);
            meta.setOffset(offset);
            resultInfo.setMetaInfo(meta);
            resultInfo.setResults(channelInfos);

        } else {
            resultInfo.setError(filterContent.getErrorText());
        }

        return resultInfo;
    }

}
