/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.youtube.aggregatorapp.services.impl;

import com.youtube.aggregatorapp.models.ResultModel;

/**
 *
 * @author subhajgh
 */
public interface ChannelInfoService {
    public  ResultModel findChannels(String filter,int limit,int offset,String sortOrder);
}
