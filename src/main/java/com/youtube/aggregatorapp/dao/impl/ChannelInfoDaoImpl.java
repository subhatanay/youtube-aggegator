/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.youtube.aggregatorapp.dao.impl;

import com.youtube.aggregatorapp.models.ChannelInfo;
import com.youtube.aggregatorapp.models.Filter;
import com.youtube.aggregatorapp.utils.FilterUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author subhajgh
 */
@Repository
public class ChannelInfoDaoImpl implements ChannelInfoDao {
    static Logger logger = LoggerFactory.getLogger(FilterUtils.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<ChannelInfo> fetchChannels(Filter filter) {
        StringBuilder sqlString = new StringBuilder("SELECT * from channel_info ");
        List<Object> obj = new ArrayList<>();
        if (filter != null) {
            if (filter.getFilterSQL()!=null) {
                sqlString.append(" where ").append(filter.getFilterSQL());
            }
            if (filter.getOrderBySQL()!=null) {
                sqlString.append(" order by ").append(filter.getOrderBySQL());
            }
            sqlString.append(" limit ?,? "); 
            obj.add(filter.getOffset()); 
            obj.add(filter.getLimit());   
            
            
        } else {
            sqlString.append(sqlString);
        }
        logger.info("Search SQL :: " + sqlString);
        List<ChannelInfo> channelInfos = new ArrayList<>();

        List<Map<String, Object>> results = jdbcTemplate.queryForList(sqlString.toString(),obj.toArray());
        results.stream().map(rows -> {
            ChannelInfo info = new ChannelInfo();
            info.setChannelName((String) rows.get("channel_name"));
            info.setRank((String) rows.get("rank"));
            info.setGrade((String) rows.get("grade"));
            info.setSubscribers(String.valueOf(rows.get("subscribers")));
            info.setVideoUploads(String.valueOf(rows.get("video_upload")));
            info.setVideoViews(String.valueOf(rows.get("video_views")));
            return info;
        }).forEach(cc -> {
            channelInfos.add(cc);
        });
        return channelInfos;
    }

    @Override
    public long totalChannels(Filter filter) {
        StringBuilder sqlString = new StringBuilder("SELECT COUNT(*) from channel_info ");
        if (filter!=null && filter.getFilterSQL()!=null) {
            sqlString.append(" where " + filter.getFilterSQL());
        }
        return jdbcTemplate.queryForObject(sqlString.toString(), Long.class);
    }

}
